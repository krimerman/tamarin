﻿namespace Tamarin
{
    partial class Call_Screen
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Dest_Panel = new System.Windows.Forms.Panel();
            this.Dest_Label1 = new Bunifu.Framework.UI.BunifuCustomLabel();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.Src_Label1 = new Bunifu.Framework.UI.BunifuCustomLabel();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.CallDur_Label1 = new Bunifu.Framework.UI.BunifuCustomLabel();
            this.bunifuCustomLabel1 = new Bunifu.Framework.UI.BunifuCustomLabel();
            this.button1 = new System.Windows.Forms.Button();
            this.Dest_Panel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.SuspendLayout();
            // 
            // Dest_Panel
            // 
            this.Dest_Panel.Controls.Add(this.Dest_Label1);
            this.Dest_Panel.Controls.Add(this.pictureBox1);
            this.Dest_Panel.Location = new System.Drawing.Point(22, 25);
            this.Dest_Panel.Name = "Dest_Panel";
            this.Dest_Panel.Size = new System.Drawing.Size(400, 400);
            this.Dest_Panel.TabIndex = 0;
            // 
            // Dest_Label1
            // 
            this.Dest_Label1.AutoSize = true;
            this.Dest_Label1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(19)))), ((int)(((byte)(33)))), ((int)(((byte)(18)))));
            this.Dest_Label1.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Dest_Label1.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.Dest_Label1.Location = new System.Drawing.Point(12, 13);
            this.Dest_Label1.Name = "Dest_Label1";
            this.Dest_Label1.Size = new System.Drawing.Size(80, 19);
            this.Dest_Label1.TabIndex = 0;
            this.Dest_Label1.Text = "Remote : ";
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(19)))), ((int)(((byte)(33)))), ((int)(((byte)(18)))));
            this.pictureBox1.Location = new System.Drawing.Point(3, 3);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(394, 394);
            this.pictureBox1.TabIndex = 3;
            this.pictureBox1.TabStop = false;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.Src_Label1);
            this.panel1.Controls.Add(this.pictureBox2);
            this.panel1.Location = new System.Drawing.Point(442, 225);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(200, 200);
            this.panel1.TabIndex = 1;
            // 
            // Src_Label1
            // 
            this.Src_Label1.AutoSize = true;
            this.Src_Label1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(19)))), ((int)(((byte)(33)))), ((int)(((byte)(18)))));
            this.Src_Label1.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Src_Label1.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.Src_Label1.Location = new System.Drawing.Point(12, 12);
            this.Src_Label1.Name = "Src_Label1";
            this.Src_Label1.Size = new System.Drawing.Size(63, 19);
            this.Src_Label1.TabIndex = 1;
            this.Src_Label1.Text = "Local : ";
            // 
            // pictureBox2
            // 
            this.pictureBox2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(19)))), ((int)(((byte)(33)))), ((int)(((byte)(18)))));
            this.pictureBox2.Location = new System.Drawing.Point(3, 3);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(194, 194);
            this.pictureBox2.TabIndex = 0;
            this.pictureBox2.TabStop = false;
            // 
            // CallDur_Label1
            // 
            this.CallDur_Label1.AutoSize = true;
            this.CallDur_Label1.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CallDur_Label1.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.CallDur_Label1.Location = new System.Drawing.Point(437, 25);
            this.CallDur_Label1.Name = "CallDur_Label1";
            this.CallDur_Label1.Size = new System.Drawing.Size(109, 19);
            this.CallDur_Label1.TabIndex = 1;
            this.CallDur_Label1.Text = "Call duration";
            // 
            // bunifuCustomLabel1
            // 
            this.bunifuCustomLabel1.AutoSize = true;
            this.bunifuCustomLabel1.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bunifuCustomLabel1.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.bunifuCustomLabel1.Location = new System.Drawing.Point(533, 53);
            this.bunifuCustomLabel1.Name = "bunifuCustomLabel1";
            this.bunifuCustomLabel1.Size = new System.Drawing.Size(71, 19);
            this.bunifuCustomLabel1.TabIndex = 2;
            this.bunifuCustomLabel1.Text = "00:00:00";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(497, 148);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 3;
            this.button1.Text = "button1";
            this.button1.UseVisualStyleBackColor = true;
            // 
            // Call_Screen
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(21)))), ((int)(((byte)(51)))), ((int)(((byte)(22)))));
            this.ClientSize = new System.Drawing.Size(665, 446);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.bunifuCustomLabel1);
            this.Controls.Add(this.CallDur_Label1);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.Dest_Panel);
            this.Name = "Call_Screen";
            this.Text = "Ongoing Call - With";
            this.Dest_Panel.ResumeLayout(false);
            this.Dest_Panel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel Dest_Panel;
        private System.Windows.Forms.Panel panel1;
        private Bunifu.Framework.UI.BunifuCustomLabel Dest_Label1;
        private Bunifu.Framework.UI.BunifuCustomLabel Src_Label1;
        private Bunifu.Framework.UI.BunifuCustomLabel CallDur_Label1;
        private Bunifu.Framework.UI.BunifuCustomLabel bunifuCustomLabel1;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.Button button1;
    }
}