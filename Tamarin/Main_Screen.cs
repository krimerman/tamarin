﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using Ozeki.Media;
using Ozeki.Network;
using Ozeki.VoIP;
using Ozeki.Common;
using Ozeki.Camera;



namespace Tamarin
{
    public partial class Main_Screen : Form
    {
        //Defining essential objects for the softphone operation
        private ISoftPhone softPhone;
        private IPhoneLine phoneLine;
        //private PhoneLineState phoneLineInformation;
        private IPhoneCall call;
       

        private Microphone microphone = Microphone.GetDefaultDevice();
        private Speaker speaker = Speaker.GetDefaultDevice();

        MediaConnector connector = new MediaConnector() ;
        PhoneCallAudioSender mediaSender = new PhoneCallAudioSender();
        PhoneCallAudioReceiver mediareceiver = new PhoneCallAudioReceiver();
        bool incommingCall = false;

        public event EventHandler IncomingCall;
        public event EventHandler<RegistrationStateChangedArgs> PhoneLineStateChanged;
        public event EventHandler<CallStateChangedArgs> CallStateChanged;

        private bool incomingCall;
        
        public Main_Screen()
        {
            InitializeComponent();
        }

        private void InitializeSoftPhone()
        {
            try
            {
                softPhone = SoftPhoneFactory.CreateSoftPhone(SoftPhoneFactory.GetLocalIP(), 5700, 5750);
                invokeGUIThread(() => { listBox1.Items.Add("Softphone Object was created"); });

                softPhone.IncomingCall += new EventHandler<VoIPEventArgs<IPhoneCall>>(softphone_IncomingCall);

                SIPAccount sa = new SIPAccount(true, "1001", "1001", "1001", "192.168.20.90", "5060");
                invokeGUIThread(() => { listBox1.Items.Add("SIP account was created for 1001"); });

                NatConfiguration nc = new NatConfiguration(NatTraversalMethod.None);
                invokeGUIThread(() => { listBox1.Items.Add("NAT Configuration created"); });

                //phoneLine = softPhone.CreatePhoneLine(sa, nc);
                phoneLine = softPhone.CreatePhoneLine(sa);
                phoneLine.RegistrationStateChanged += phoneLine_PhoneLineInformation;
                invokeGUIThread(() => { listBox1.Items.Add("Phone Line Created created"); });
                softPhone.RegisterPhoneLine(phoneLine);
            }
            catch (Exception ex)
            {
                invokeGUIThread(() => { listBox1.Items.Add("EXCEPTION - exception was encountered - local IP error"); });
            }
        }




        public void invokeGUIThread (Action action)
        {
            Invoke(action);
        }

        private void softphone_IncomingCall(object sender, VoIPEventArgs<IPhoneCall> e)
        {
            invokeGUIThread(() => { listBox1.Items.Add("Incoming Call - From : " + e.Item.DialInfo.ToString()); });
        }

        private void phoneLine_PhoneLineInformation (object sender , RegistrationStateChangedArgs e)
        {
            //phoneLineInformation = e.State;
            invokeGUIThread(() => {
                if (e.State == RegState.Error || e.State == RegState.NotRegistered)
                {
                    listBox1.Items.Add("Registration failed or did not occur - not online");
                }
                else if (e.State == RegState.RegistrationSucceeded)
                {
                    listBox1.Items.Add("Registration failed - not online" + e.State.ToString());
                }
                else
                {
                    listBox1.Items.Add("Registration - something else : " + e.State.ToString());
                }
            });

        }





        private void call_CallErrorOccurred (object sender , CallErrorEventArgs e)
        {
            invokeGUIThread(() => { listBox1.Items.Add("Call error occurred" + e.Reason); });
        }

        private void call_CallStateChanged (object sender, CallStateChangedArgs e)
        {
            invokeGUIThread(() => { listBox1.Items.Add("Call state changed: " + e.State.ToString()); });
        }

        private void WireUpCallEvents ()
        {
            call.CallStateChanged += (call_CallStateChanged);
            //call. += (call_CallErrorOccurred);
        }

        private void WireDownCallEvents ()
        {
            call.CallStateChanged -= (call_CallStateChanged);
            //call.CallErrorOccurred -= (call_CallErrorOccurred);
        }












        private void top_btn_dpad_Click(object sender, EventArgs e)
        {
            panel_Log.Visible = false;
            panel_History.Visible = false;
            panel_Dpad.Visible = true;
            panel_Dpad.BringToFront();
        }

        private void top_btn_hist_Click(object sender, EventArgs e)
        {
            panel_Log.Visible = false;
            panel_Dpad.Visible = false;
            panel_History.Visible = true;
            panel_History.BringToFront();
        }

        private void top_btn_cont_Click(object sender, EventArgs e)
        {

        }

        private void top_btn_log_Click(object sender, EventArgs e)
        {
            panel_Dpad.Visible = false;
            panel_History.Visible = false;
            panel_Log.Visible = true;
            panel_Log.BringToFront();
        }

        private void top_btn_set_Click(object sender, EventArgs e)
        {

        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {

        }

        
        private void Main_Screen_Load(object sender, EventArgs e)
        {
            InitializeSoftPhone();
        }

        private void bunifuCustomLabel1_Click(object sender, EventArgs e)
        {

        }

        private void bunifuCustomLabel4_Click(object sender, EventArgs e)
        {

        }

        private void bunifuMetroTextbox3_OnValueChanged(object sender, EventArgs e)
        {

        }

        private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void Button_Dpad_Click(object sender, EventArgs e)
        {
            var btn = sender as Button;
            if (call != null) { return; };
            if (btn == null) { return; };

            bunifuMaterialTextbox1.Text += btn.Text.Trim();
        }


    }
}
