﻿namespace Tamarin
{
    partial class Main_Screen
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Main_Screen));
            this.panel_icons = new System.Windows.Forms.Panel();
            this.Top_btn_set = new Bunifu.Framework.UI.BunifuFlatButton();
            this.Top_btn_log = new Bunifu.Framework.UI.BunifuFlatButton();
            this.Top_btn_cont = new Bunifu.Framework.UI.BunifuFlatButton();
            this.Top_btn_hist = new Bunifu.Framework.UI.BunifuFlatButton();
            this.Top_btn_dpad = new Bunifu.Framework.UI.BunifuFlatButton();
            this.panel_status = new System.Windows.Forms.Panel();
            this.panel_Login = new System.Windows.Forms.Panel();
            this.RegButtin = new Bunifu.Framework.UI.BunifuThinButton2();
            this.bunifuMetroTextbox3 = new Bunifu.Framework.UI.BunifuMetroTextbox();
            this.bunifuCustomLabel4 = new Bunifu.Framework.UI.BunifuCustomLabel();
            this.bunifuMetroTextbox2 = new Bunifu.Framework.UI.BunifuMetroTextbox();
            this.bunifuMetroTextbox1 = new Bunifu.Framework.UI.BunifuMetroTextbox();
            this.bunifuCustomLabel3 = new Bunifu.Framework.UI.BunifuCustomLabel();
            this.bunifuCustomLabel2 = new Bunifu.Framework.UI.BunifuCustomLabel();
            this.bunifuCustomLabel1 = new Bunifu.Framework.UI.BunifuCustomLabel();
            this.Button_Dpad_1 = new Bunifu.Framework.UI.BunifuFlatButton();
            this.Button_Dpad_2 = new Bunifu.Framework.UI.BunifuFlatButton();
            this.Button_Dpad_3 = new Bunifu.Framework.UI.BunifuFlatButton();
            this.Button_Dpad_4 = new Bunifu.Framework.UI.BunifuFlatButton();
            this.Button_Dpad_5 = new Bunifu.Framework.UI.BunifuFlatButton();
            this.Button_Dpad_6 = new Bunifu.Framework.UI.BunifuFlatButton();
            this.Button_Dpad_7 = new Bunifu.Framework.UI.BunifuFlatButton();
            this.Button_Dpad_8 = new Bunifu.Framework.UI.BunifuFlatButton();
            this.Button_Dpad_9 = new Bunifu.Framework.UI.BunifuFlatButton();
            this.Button_Dpad_star = new Bunifu.Framework.UI.BunifuFlatButton();
            this.Button_Dpad_0 = new Bunifu.Framework.UI.BunifuFlatButton();
            this.Button_Dpad_hash = new Bunifu.Framework.UI.BunifuFlatButton();
            this.bunifuFlatButton15 = new Bunifu.Framework.UI.BunifuFlatButton();
            this.bunifuFlatButton14 = new Bunifu.Framework.UI.BunifuFlatButton();
            this.bunifuFlatButton13 = new Bunifu.Framework.UI.BunifuFlatButton();
            this.panel_Dpad = new System.Windows.Forms.Panel();
            this.bunifuMaterialTextbox1 = new Bunifu.Framework.UI.BunifuMaterialTextbox();
            this.panel_Log = new System.Windows.Forms.Panel();
            this.listBox1 = new System.Windows.Forms.ListBox();
            this.panel_History = new System.Windows.Forms.Panel();
            this.bunifu_Panel_Bottom = new Bunifu.Framework.UI.BunifuGradientPanel();
            this.listBox_History = new System.Windows.Forms.ListBox();
            this.panel_icons.SuspendLayout();
            this.panel_status.SuspendLayout();
            this.panel_Login.SuspendLayout();
            this.panel_Dpad.SuspendLayout();
            this.panel_Log.SuspendLayout();
            this.panel_History.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel_icons
            // 
            this.panel_icons.Controls.Add(this.Top_btn_set);
            this.panel_icons.Controls.Add(this.Top_btn_log);
            this.panel_icons.Controls.Add(this.Top_btn_cont);
            this.panel_icons.Controls.Add(this.Top_btn_hist);
            this.panel_icons.Controls.Add(this.Top_btn_dpad);
            this.panel_icons.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.panel_icons.Location = new System.Drawing.Point(-1, 167);
            this.panel_icons.Name = "panel_icons";
            this.panel_icons.Size = new System.Drawing.Size(405, 71);
            this.panel_icons.TabIndex = 0;
            this.panel_icons.Paint += new System.Windows.Forms.PaintEventHandler(this.panel1_Paint);
            // 
            // Top_btn_set
            // 
            this.Top_btn_set.Activecolor = System.Drawing.Color.Chartreuse;
            this.Top_btn_set.BackColor = System.Drawing.SystemColors.Control;
            this.Top_btn_set.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.Top_btn_set.BorderRadius = 0;
            this.Top_btn_set.ButtonText = "";
            this.Top_btn_set.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Top_btn_set.DisabledColor = System.Drawing.Color.Gray;
            this.Top_btn_set.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Top_btn_set.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.Top_btn_set.Iconcolor = System.Drawing.Color.Transparent;
            this.Top_btn_set.Iconimage = ((System.Drawing.Image)(resources.GetObject("Top_btn_set.Iconimage")));
            this.Top_btn_set.Iconimage_right = null;
            this.Top_btn_set.Iconimage_right_Selected = null;
            this.Top_btn_set.Iconimage_Selected = null;
            this.Top_btn_set.IconMarginLeft = 0;
            this.Top_btn_set.IconMarginRight = 0;
            this.Top_btn_set.IconRightVisible = true;
            this.Top_btn_set.IconRightZoom = 0D;
            this.Top_btn_set.IconVisible = true;
            this.Top_btn_set.IconZoom = 90D;
            this.Top_btn_set.IsTab = false;
            this.Top_btn_set.Location = new System.Drawing.Point(331, 10);
            this.Top_btn_set.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Top_btn_set.Name = "Top_btn_set";
            this.Top_btn_set.Normalcolor = System.Drawing.SystemColors.Control;
            this.Top_btn_set.OnHovercolor = System.Drawing.Color.DarkSeaGreen;
            this.Top_btn_set.OnHoverTextColor = System.Drawing.Color.White;
            this.Top_btn_set.selected = false;
            this.Top_btn_set.Size = new System.Drawing.Size(50, 50);
            this.Top_btn_set.TabIndex = 4;
            this.Top_btn_set.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.Top_btn_set.Textcolor = System.Drawing.Color.White;
            this.Top_btn_set.TextFont = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Top_btn_set.Click += new System.EventHandler(this.top_btn_set_Click);
            // 
            // Top_btn_log
            // 
            this.Top_btn_log.Activecolor = System.Drawing.Color.Chartreuse;
            this.Top_btn_log.BackColor = System.Drawing.SystemColors.Control;
            this.Top_btn_log.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.Top_btn_log.BorderRadius = 0;
            this.Top_btn_log.ButtonText = "";
            this.Top_btn_log.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Top_btn_log.DisabledColor = System.Drawing.Color.Gray;
            this.Top_btn_log.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Top_btn_log.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.Top_btn_log.Iconcolor = System.Drawing.Color.Transparent;
            this.Top_btn_log.Iconimage = ((System.Drawing.Image)(resources.GetObject("Top_btn_log.Iconimage")));
            this.Top_btn_log.Iconimage_right = null;
            this.Top_btn_log.Iconimage_right_Selected = null;
            this.Top_btn_log.Iconimage_Selected = null;
            this.Top_btn_log.IconMarginLeft = 0;
            this.Top_btn_log.IconMarginRight = 0;
            this.Top_btn_log.IconRightVisible = true;
            this.Top_btn_log.IconRightZoom = 0D;
            this.Top_btn_log.IconVisible = true;
            this.Top_btn_log.IconZoom = 90D;
            this.Top_btn_log.IsTab = false;
            this.Top_btn_log.Location = new System.Drawing.Point(253, 10);
            this.Top_btn_log.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Top_btn_log.Name = "Top_btn_log";
            this.Top_btn_log.Normalcolor = System.Drawing.SystemColors.Control;
            this.Top_btn_log.OnHovercolor = System.Drawing.Color.DarkSeaGreen;
            this.Top_btn_log.OnHoverTextColor = System.Drawing.Color.White;
            this.Top_btn_log.selected = false;
            this.Top_btn_log.Size = new System.Drawing.Size(50, 50);
            this.Top_btn_log.TabIndex = 3;
            this.Top_btn_log.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.Top_btn_log.Textcolor = System.Drawing.Color.White;
            this.Top_btn_log.TextFont = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Top_btn_log.Click += new System.EventHandler(this.top_btn_log_Click);
            // 
            // Top_btn_cont
            // 
            this.Top_btn_cont.Activecolor = System.Drawing.Color.Chartreuse;
            this.Top_btn_cont.BackColor = System.Drawing.SystemColors.Control;
            this.Top_btn_cont.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.Top_btn_cont.BorderRadius = 0;
            this.Top_btn_cont.ButtonText = "";
            this.Top_btn_cont.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Top_btn_cont.DisabledColor = System.Drawing.Color.Gray;
            this.Top_btn_cont.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Top_btn_cont.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.Top_btn_cont.Iconcolor = System.Drawing.Color.Transparent;
            this.Top_btn_cont.Iconimage = ((System.Drawing.Image)(resources.GetObject("Top_btn_cont.Iconimage")));
            this.Top_btn_cont.Iconimage_right = null;
            this.Top_btn_cont.Iconimage_right_Selected = null;
            this.Top_btn_cont.Iconimage_Selected = null;
            this.Top_btn_cont.IconMarginLeft = 0;
            this.Top_btn_cont.IconMarginRight = 0;
            this.Top_btn_cont.IconRightVisible = true;
            this.Top_btn_cont.IconRightZoom = 0D;
            this.Top_btn_cont.IconVisible = true;
            this.Top_btn_cont.IconZoom = 90D;
            this.Top_btn_cont.IsTab = false;
            this.Top_btn_cont.Location = new System.Drawing.Point(175, 10);
            this.Top_btn_cont.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Top_btn_cont.Name = "Top_btn_cont";
            this.Top_btn_cont.Normalcolor = System.Drawing.SystemColors.Control;
            this.Top_btn_cont.OnHovercolor = System.Drawing.Color.DarkSeaGreen;
            this.Top_btn_cont.OnHoverTextColor = System.Drawing.Color.White;
            this.Top_btn_cont.selected = false;
            this.Top_btn_cont.Size = new System.Drawing.Size(50, 50);
            this.Top_btn_cont.TabIndex = 2;
            this.Top_btn_cont.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.Top_btn_cont.Textcolor = System.Drawing.Color.White;
            this.Top_btn_cont.TextFont = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Top_btn_cont.Click += new System.EventHandler(this.top_btn_cont_Click);
            // 
            // Top_btn_hist
            // 
            this.Top_btn_hist.Activecolor = System.Drawing.Color.Chartreuse;
            this.Top_btn_hist.BackColor = System.Drawing.SystemColors.Control;
            this.Top_btn_hist.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.Top_btn_hist.BorderRadius = 0;
            this.Top_btn_hist.ButtonText = "";
            this.Top_btn_hist.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Top_btn_hist.DisabledColor = System.Drawing.Color.Gray;
            this.Top_btn_hist.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Top_btn_hist.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.Top_btn_hist.Iconcolor = System.Drawing.Color.Transparent;
            this.Top_btn_hist.Iconimage = ((System.Drawing.Image)(resources.GetObject("Top_btn_hist.Iconimage")));
            this.Top_btn_hist.Iconimage_right = null;
            this.Top_btn_hist.Iconimage_right_Selected = null;
            this.Top_btn_hist.Iconimage_Selected = null;
            this.Top_btn_hist.IconMarginLeft = 0;
            this.Top_btn_hist.IconMarginRight = 0;
            this.Top_btn_hist.IconRightVisible = true;
            this.Top_btn_hist.IconRightZoom = 0D;
            this.Top_btn_hist.IconVisible = true;
            this.Top_btn_hist.IconZoom = 90D;
            this.Top_btn_hist.IsTab = false;
            this.Top_btn_hist.Location = new System.Drawing.Point(97, 10);
            this.Top_btn_hist.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Top_btn_hist.Name = "Top_btn_hist";
            this.Top_btn_hist.Normalcolor = System.Drawing.SystemColors.Control;
            this.Top_btn_hist.OnHovercolor = System.Drawing.Color.DarkSeaGreen;
            this.Top_btn_hist.OnHoverTextColor = System.Drawing.Color.White;
            this.Top_btn_hist.selected = false;
            this.Top_btn_hist.Size = new System.Drawing.Size(56, 50);
            this.Top_btn_hist.TabIndex = 1;
            this.Top_btn_hist.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.Top_btn_hist.Textcolor = System.Drawing.Color.White;
            this.Top_btn_hist.TextFont = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Top_btn_hist.Click += new System.EventHandler(this.top_btn_hist_Click);
            // 
            // Top_btn_dpad
            // 
            this.Top_btn_dpad.Activecolor = System.Drawing.Color.Chartreuse;
            this.Top_btn_dpad.BackColor = System.Drawing.SystemColors.Control;
            this.Top_btn_dpad.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.Top_btn_dpad.BorderRadius = 0;
            this.Top_btn_dpad.ButtonText = "Dialpad";
            this.Top_btn_dpad.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Top_btn_dpad.DisabledColor = System.Drawing.Color.Gray;
            this.Top_btn_dpad.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Top_btn_dpad.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.Top_btn_dpad.Iconcolor = System.Drawing.Color.Transparent;
            this.Top_btn_dpad.Iconimage = ((System.Drawing.Image)(resources.GetObject("Top_btn_dpad.Iconimage")));
            this.Top_btn_dpad.Iconimage_right = null;
            this.Top_btn_dpad.Iconimage_right_Selected = null;
            this.Top_btn_dpad.Iconimage_Selected = null;
            this.Top_btn_dpad.IconMarginLeft = 0;
            this.Top_btn_dpad.IconMarginRight = 0;
            this.Top_btn_dpad.IconRightVisible = true;
            this.Top_btn_dpad.IconRightZoom = 0D;
            this.Top_btn_dpad.IconVisible = true;
            this.Top_btn_dpad.IconZoom = 90D;
            this.Top_btn_dpad.IsTab = false;
            this.Top_btn_dpad.Location = new System.Drawing.Point(19, 10);
            this.Top_btn_dpad.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Top_btn_dpad.Name = "Top_btn_dpad";
            this.Top_btn_dpad.Normalcolor = System.Drawing.SystemColors.Control;
            this.Top_btn_dpad.OnHovercolor = System.Drawing.Color.DarkSeaGreen;
            this.Top_btn_dpad.OnHoverTextColor = System.Drawing.Color.Black;
            this.Top_btn_dpad.selected = false;
            this.Top_btn_dpad.Size = new System.Drawing.Size(50, 50);
            this.Top_btn_dpad.TabIndex = 0;
            this.Top_btn_dpad.Tag = "Dialpad";
            this.Top_btn_dpad.Text = "Dialpad";
            this.Top_btn_dpad.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.Top_btn_dpad.Textcolor = System.Drawing.Color.Black;
            this.Top_btn_dpad.TextFont = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Top_btn_dpad.Click += new System.EventHandler(this.top_btn_dpad_Click);
            // 
            // panel_status
            // 
            this.panel_status.BackColor = System.Drawing.Color.DarkSeaGreen;
            this.panel_status.Controls.Add(this.panel_Login);
            this.panel_status.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.panel_status.Location = new System.Drawing.Point(-1, 70);
            this.panel_status.Name = "panel_status";
            this.panel_status.Size = new System.Drawing.Size(405, 94);
            this.panel_status.TabIndex = 1;
            // 
            // panel_Login
            // 
            this.panel_Login.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(56)))), ((int)(((byte)(116)))), ((int)(((byte)(80)))));
            this.panel_Login.Controls.Add(this.RegButtin);
            this.panel_Login.Controls.Add(this.bunifuMetroTextbox3);
            this.panel_Login.Controls.Add(this.bunifuCustomLabel4);
            this.panel_Login.Controls.Add(this.bunifuMetroTextbox2);
            this.panel_Login.Controls.Add(this.bunifuMetroTextbox1);
            this.panel_Login.Controls.Add(this.bunifuCustomLabel3);
            this.panel_Login.Controls.Add(this.bunifuCustomLabel2);
            this.panel_Login.Controls.Add(this.bunifuCustomLabel1);
            this.panel_Login.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.panel_Login.Location = new System.Drawing.Point(0, 0);
            this.panel_Login.Name = "panel_Login";
            this.panel_Login.Size = new System.Drawing.Size(391, 94);
            this.panel_Login.TabIndex = 2;
            // 
            // RegButtin
            // 
            this.RegButtin.ActiveBorderThickness = 1;
            this.RegButtin.ActiveCornerRadius = 20;
            this.RegButtin.ActiveFillColor = System.Drawing.Color.SeaGreen;
            this.RegButtin.ActiveForecolor = System.Drawing.Color.White;
            this.RegButtin.ActiveLineColor = System.Drawing.Color.SeaGreen;
            this.RegButtin.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(56)))), ((int)(((byte)(116)))), ((int)(((byte)(80)))));
            this.RegButtin.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("RegButtin.BackgroundImage")));
            this.RegButtin.ButtonText = "Register";
            this.RegButtin.Cursor = System.Windows.Forms.Cursors.Hand;
            this.RegButtin.Font = new System.Drawing.Font("Century Gothic", 8F);
            this.RegButtin.ForeColor = System.Drawing.Color.White;
            this.RegButtin.IdleBorderThickness = 1;
            this.RegButtin.IdleCornerRadius = 20;
            this.RegButtin.IdleFillColor = System.Drawing.Color.Transparent;
            this.RegButtin.IdleForecolor = System.Drawing.Color.SeaGreen;
            this.RegButtin.IdleLineColor = System.Drawing.Color.White;
            this.RegButtin.Location = new System.Drawing.Point(246, 55);
            this.RegButtin.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.RegButtin.Name = "RegButtin";
            this.RegButtin.Size = new System.Drawing.Size(108, 34);
            this.RegButtin.TabIndex = 7;
            this.RegButtin.TabStop = false;
            this.RegButtin.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // bunifuMetroTextbox3
            // 
            this.bunifuMetroTextbox3.BorderColorFocused = System.Drawing.Color.Blue;
            this.bunifuMetroTextbox3.BorderColorIdle = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.bunifuMetroTextbox3.BorderColorMouseHover = System.Drawing.Color.Blue;
            this.bunifuMetroTextbox3.BorderThickness = 3;
            this.bunifuMetroTextbox3.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.bunifuMetroTextbox3.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bunifuMetroTextbox3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.bunifuMetroTextbox3.isPassword = false;
            this.bunifuMetroTextbox3.Location = new System.Drawing.Point(241, 30);
            this.bunifuMetroTextbox3.Margin = new System.Windows.Forms.Padding(4);
            this.bunifuMetroTextbox3.Name = "bunifuMetroTextbox3";
            this.bunifuMetroTextbox3.Size = new System.Drawing.Size(122, 25);
            this.bunifuMetroTextbox3.TabIndex = 6;
            this.bunifuMetroTextbox3.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.bunifuMetroTextbox3.OnValueChanged += new System.EventHandler(this.bunifuMetroTextbox3_OnValueChanged);
            // 
            // bunifuCustomLabel4
            // 
            this.bunifuCustomLabel4.AutoSize = true;
            this.bunifuCustomLabel4.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bunifuCustomLabel4.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.bunifuCustomLabel4.Location = new System.Drawing.Point(188, 34);
            this.bunifuCustomLabel4.Name = "bunifuCustomLabel4";
            this.bunifuCustomLabel4.Size = new System.Drawing.Size(51, 16);
            this.bunifuCustomLabel4.TabIndex = 5;
            this.bunifuCustomLabel4.Text = "Server : ";
            this.bunifuCustomLabel4.Click += new System.EventHandler(this.bunifuCustomLabel4_Click);
            // 
            // bunifuMetroTextbox2
            // 
            this.bunifuMetroTextbox2.BorderColorFocused = System.Drawing.Color.Blue;
            this.bunifuMetroTextbox2.BorderColorIdle = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.bunifuMetroTextbox2.BorderColorMouseHover = System.Drawing.Color.Blue;
            this.bunifuMetroTextbox2.BorderThickness = 3;
            this.bunifuMetroTextbox2.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.bunifuMetroTextbox2.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bunifuMetroTextbox2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.bunifuMetroTextbox2.isPassword = false;
            this.bunifuMetroTextbox2.Location = new System.Drawing.Point(61, 57);
            this.bunifuMetroTextbox2.Margin = new System.Windows.Forms.Padding(4);
            this.bunifuMetroTextbox2.Name = "bunifuMetroTextbox2";
            this.bunifuMetroTextbox2.Size = new System.Drawing.Size(122, 25);
            this.bunifuMetroTextbox2.TabIndex = 4;
            this.bunifuMetroTextbox2.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            // 
            // bunifuMetroTextbox1
            // 
            this.bunifuMetroTextbox1.BorderColorFocused = System.Drawing.Color.Blue;
            this.bunifuMetroTextbox1.BorderColorIdle = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.bunifuMetroTextbox1.BorderColorMouseHover = System.Drawing.Color.Blue;
            this.bunifuMetroTextbox1.BorderThickness = 3;
            this.bunifuMetroTextbox1.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.bunifuMetroTextbox1.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bunifuMetroTextbox1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.bunifuMetroTextbox1.isPassword = false;
            this.bunifuMetroTextbox1.Location = new System.Drawing.Point(61, 30);
            this.bunifuMetroTextbox1.Margin = new System.Windows.Forms.Padding(4);
            this.bunifuMetroTextbox1.Name = "bunifuMetroTextbox1";
            this.bunifuMetroTextbox1.Size = new System.Drawing.Size(122, 25);
            this.bunifuMetroTextbox1.TabIndex = 3;
            this.bunifuMetroTextbox1.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            // 
            // bunifuCustomLabel3
            // 
            this.bunifuCustomLabel3.AutoSize = true;
            this.bunifuCustomLabel3.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bunifuCustomLabel3.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.bunifuCustomLabel3.Location = new System.Drawing.Point(8, 62);
            this.bunifuCustomLabel3.Name = "bunifuCustomLabel3";
            this.bunifuCustomLabel3.Size = new System.Drawing.Size(56, 16);
            this.bunifuCustomLabel3.TabIndex = 2;
            this.bunifuCustomLabel3.Text = "SIP Pass :";
            // 
            // bunifuCustomLabel2
            // 
            this.bunifuCustomLabel2.AutoSize = true;
            this.bunifuCustomLabel2.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bunifuCustomLabel2.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.bunifuCustomLabel2.Location = new System.Drawing.Point(8, 34);
            this.bunifuCustomLabel2.Name = "bunifuCustomLabel2";
            this.bunifuCustomLabel2.Size = new System.Drawing.Size(55, 16);
            this.bunifuCustomLabel2.TabIndex = 1;
            this.bunifuCustomLabel2.Text = "SIP User :";
            // 
            // bunifuCustomLabel1
            // 
            this.bunifuCustomLabel1.AutoSize = true;
            this.bunifuCustomLabel1.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bunifuCustomLabel1.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.bunifuCustomLabel1.Location = new System.Drawing.Point(8, 10);
            this.bunifuCustomLabel1.Name = "bunifuCustomLabel1";
            this.bunifuCustomLabel1.Size = new System.Drawing.Size(238, 16);
            this.bunifuCustomLabel1.TabIndex = 0;
            this.bunifuCustomLabel1.Text = "Please provide credentials for Registration :";
            this.bunifuCustomLabel1.Click += new System.EventHandler(this.bunifuCustomLabel1_Click);
            // 
            // Button_Dpad_1
            // 
            this.Button_Dpad_1.Activecolor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(139)))), ((int)(((byte)(87)))));
            this.Button_Dpad_1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.Button_Dpad_1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.Button_Dpad_1.BorderRadius = 0;
            this.Button_Dpad_1.ButtonText = "1";
            this.Button_Dpad_1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Button_Dpad_1.DisabledColor = System.Drawing.Color.Gray;
            this.Button_Dpad_1.Font = new System.Drawing.Font("Bell MT", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Button_Dpad_1.Iconcolor = System.Drawing.Color.Transparent;
            this.Button_Dpad_1.Iconimage = null;
            this.Button_Dpad_1.Iconimage_right = null;
            this.Button_Dpad_1.Iconimage_right_Selected = null;
            this.Button_Dpad_1.Iconimage_Selected = null;
            this.Button_Dpad_1.IconMarginLeft = 0;
            this.Button_Dpad_1.IconMarginRight = 0;
            this.Button_Dpad_1.IconRightVisible = true;
            this.Button_Dpad_1.IconRightZoom = 0D;
            this.Button_Dpad_1.IconVisible = true;
            this.Button_Dpad_1.IconZoom = 90D;
            this.Button_Dpad_1.IsTab = false;
            this.Button_Dpad_1.Location = new System.Drawing.Point(54, 87);
            this.Button_Dpad_1.Name = "Button_Dpad_1";
            this.Button_Dpad_1.Normalcolor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.Button_Dpad_1.OnHovercolor = System.Drawing.Color.DarkSeaGreen;
            this.Button_Dpad_1.OnHoverTextColor = System.Drawing.Color.Black;
            this.Button_Dpad_1.selected = false;
            this.Button_Dpad_1.Size = new System.Drawing.Size(94, 48);
            this.Button_Dpad_1.TabIndex = 0;
            this.Button_Dpad_1.Text = "1";
            this.Button_Dpad_1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.Button_Dpad_1.Textcolor = System.Drawing.Color.Black;
            this.Button_Dpad_1.TextFont = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Button_Dpad_1.Click += new System.EventHandler(this.Button_Dpad_Click);
            // 
            // Button_Dpad_2
            // 
            this.Button_Dpad_2.Activecolor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(139)))), ((int)(((byte)(87)))));
            this.Button_Dpad_2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.Button_Dpad_2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.Button_Dpad_2.BorderRadius = 0;
            this.Button_Dpad_2.ButtonText = "2";
            this.Button_Dpad_2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Button_Dpad_2.DisabledColor = System.Drawing.Color.Gray;
            this.Button_Dpad_2.Font = new System.Drawing.Font("Bell MT", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Button_Dpad_2.Iconcolor = System.Drawing.Color.Transparent;
            this.Button_Dpad_2.Iconimage = null;
            this.Button_Dpad_2.Iconimage_right = null;
            this.Button_Dpad_2.Iconimage_right_Selected = null;
            this.Button_Dpad_2.Iconimage_Selected = null;
            this.Button_Dpad_2.IconMarginLeft = 0;
            this.Button_Dpad_2.IconMarginRight = 0;
            this.Button_Dpad_2.IconRightVisible = true;
            this.Button_Dpad_2.IconRightZoom = 0D;
            this.Button_Dpad_2.IconVisible = true;
            this.Button_Dpad_2.IconZoom = 90D;
            this.Button_Dpad_2.IsTab = false;
            this.Button_Dpad_2.Location = new System.Drawing.Point(154, 87);
            this.Button_Dpad_2.Name = "Button_Dpad_2";
            this.Button_Dpad_2.Normalcolor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.Button_Dpad_2.OnHovercolor = System.Drawing.Color.DarkSeaGreen;
            this.Button_Dpad_2.OnHoverTextColor = System.Drawing.Color.Black;
            this.Button_Dpad_2.selected = false;
            this.Button_Dpad_2.Size = new System.Drawing.Size(94, 48);
            this.Button_Dpad_2.TabIndex = 1;
            this.Button_Dpad_2.Text = "2";
            this.Button_Dpad_2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.Button_Dpad_2.Textcolor = System.Drawing.Color.Black;
            this.Button_Dpad_2.TextFont = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Button_Dpad_2.Click += new System.EventHandler(this.Button_Dpad_Click);
            // 
            // Button_Dpad_3
            // 
            this.Button_Dpad_3.Activecolor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(139)))), ((int)(((byte)(87)))));
            this.Button_Dpad_3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.Button_Dpad_3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.Button_Dpad_3.BorderRadius = 0;
            this.Button_Dpad_3.ButtonText = "3";
            this.Button_Dpad_3.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Button_Dpad_3.DisabledColor = System.Drawing.Color.Gray;
            this.Button_Dpad_3.Font = new System.Drawing.Font("Bell MT", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Button_Dpad_3.Iconcolor = System.Drawing.Color.Transparent;
            this.Button_Dpad_3.Iconimage = null;
            this.Button_Dpad_3.Iconimage_right = null;
            this.Button_Dpad_3.Iconimage_right_Selected = null;
            this.Button_Dpad_3.Iconimage_Selected = null;
            this.Button_Dpad_3.IconMarginLeft = 0;
            this.Button_Dpad_3.IconMarginRight = 0;
            this.Button_Dpad_3.IconRightVisible = true;
            this.Button_Dpad_3.IconRightZoom = 0D;
            this.Button_Dpad_3.IconVisible = true;
            this.Button_Dpad_3.IconZoom = 90D;
            this.Button_Dpad_3.IsTab = false;
            this.Button_Dpad_3.Location = new System.Drawing.Point(254, 87);
            this.Button_Dpad_3.Name = "Button_Dpad_3";
            this.Button_Dpad_3.Normalcolor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.Button_Dpad_3.OnHovercolor = System.Drawing.Color.DarkSeaGreen;
            this.Button_Dpad_3.OnHoverTextColor = System.Drawing.Color.Black;
            this.Button_Dpad_3.selected = false;
            this.Button_Dpad_3.Size = new System.Drawing.Size(94, 48);
            this.Button_Dpad_3.TabIndex = 2;
            this.Button_Dpad_3.Text = "3";
            this.Button_Dpad_3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.Button_Dpad_3.Textcolor = System.Drawing.Color.Black;
            this.Button_Dpad_3.TextFont = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Button_Dpad_3.Click += new System.EventHandler(this.Button_Dpad_Click);
            // 
            // Button_Dpad_4
            // 
            this.Button_Dpad_4.Activecolor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(139)))), ((int)(((byte)(87)))));
            this.Button_Dpad_4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.Button_Dpad_4.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.Button_Dpad_4.BorderRadius = 0;
            this.Button_Dpad_4.ButtonText = "4";
            this.Button_Dpad_4.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Button_Dpad_4.DisabledColor = System.Drawing.Color.Gray;
            this.Button_Dpad_4.Font = new System.Drawing.Font("Bell MT", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Button_Dpad_4.Iconcolor = System.Drawing.Color.Transparent;
            this.Button_Dpad_4.Iconimage = null;
            this.Button_Dpad_4.Iconimage_right = null;
            this.Button_Dpad_4.Iconimage_right_Selected = null;
            this.Button_Dpad_4.Iconimage_Selected = null;
            this.Button_Dpad_4.IconMarginLeft = 0;
            this.Button_Dpad_4.IconMarginRight = 0;
            this.Button_Dpad_4.IconRightVisible = true;
            this.Button_Dpad_4.IconRightZoom = 0D;
            this.Button_Dpad_4.IconVisible = true;
            this.Button_Dpad_4.IconZoom = 90D;
            this.Button_Dpad_4.IsTab = false;
            this.Button_Dpad_4.Location = new System.Drawing.Point(54, 141);
            this.Button_Dpad_4.Name = "Button_Dpad_4";
            this.Button_Dpad_4.Normalcolor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.Button_Dpad_4.OnHovercolor = System.Drawing.Color.DarkSeaGreen;
            this.Button_Dpad_4.OnHoverTextColor = System.Drawing.Color.Black;
            this.Button_Dpad_4.selected = false;
            this.Button_Dpad_4.Size = new System.Drawing.Size(94, 48);
            this.Button_Dpad_4.TabIndex = 3;
            this.Button_Dpad_4.Text = "4";
            this.Button_Dpad_4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.Button_Dpad_4.Textcolor = System.Drawing.Color.Black;
            this.Button_Dpad_4.TextFont = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Button_Dpad_4.Click += new System.EventHandler(this.Button_Dpad_Click);
            // 
            // Button_Dpad_5
            // 
            this.Button_Dpad_5.Activecolor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(139)))), ((int)(((byte)(87)))));
            this.Button_Dpad_5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.Button_Dpad_5.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.Button_Dpad_5.BorderRadius = 0;
            this.Button_Dpad_5.ButtonText = "5";
            this.Button_Dpad_5.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Button_Dpad_5.DisabledColor = System.Drawing.Color.Gray;
            this.Button_Dpad_5.Font = new System.Drawing.Font("Bell MT", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Button_Dpad_5.Iconcolor = System.Drawing.Color.Transparent;
            this.Button_Dpad_5.Iconimage = null;
            this.Button_Dpad_5.Iconimage_right = null;
            this.Button_Dpad_5.Iconimage_right_Selected = null;
            this.Button_Dpad_5.Iconimage_Selected = null;
            this.Button_Dpad_5.IconMarginLeft = 0;
            this.Button_Dpad_5.IconMarginRight = 0;
            this.Button_Dpad_5.IconRightVisible = true;
            this.Button_Dpad_5.IconRightZoom = 0D;
            this.Button_Dpad_5.IconVisible = true;
            this.Button_Dpad_5.IconZoom = 90D;
            this.Button_Dpad_5.IsTab = false;
            this.Button_Dpad_5.Location = new System.Drawing.Point(154, 141);
            this.Button_Dpad_5.Name = "Button_Dpad_5";
            this.Button_Dpad_5.Normalcolor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.Button_Dpad_5.OnHovercolor = System.Drawing.Color.DarkSeaGreen;
            this.Button_Dpad_5.OnHoverTextColor = System.Drawing.Color.Black;
            this.Button_Dpad_5.selected = false;
            this.Button_Dpad_5.Size = new System.Drawing.Size(94, 48);
            this.Button_Dpad_5.TabIndex = 4;
            this.Button_Dpad_5.Text = "5";
            this.Button_Dpad_5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.Button_Dpad_5.Textcolor = System.Drawing.Color.Black;
            this.Button_Dpad_5.TextFont = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Button_Dpad_5.Click += new System.EventHandler(this.Button_Dpad_Click);
            // 
            // Button_Dpad_6
            // 
            this.Button_Dpad_6.Activecolor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(139)))), ((int)(((byte)(87)))));
            this.Button_Dpad_6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.Button_Dpad_6.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.Button_Dpad_6.BorderRadius = 0;
            this.Button_Dpad_6.ButtonText = "6";
            this.Button_Dpad_6.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Button_Dpad_6.DisabledColor = System.Drawing.Color.Gray;
            this.Button_Dpad_6.Font = new System.Drawing.Font("Bell MT", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Button_Dpad_6.Iconcolor = System.Drawing.Color.Transparent;
            this.Button_Dpad_6.Iconimage = null;
            this.Button_Dpad_6.Iconimage_right = null;
            this.Button_Dpad_6.Iconimage_right_Selected = null;
            this.Button_Dpad_6.Iconimage_Selected = null;
            this.Button_Dpad_6.IconMarginLeft = 0;
            this.Button_Dpad_6.IconMarginRight = 0;
            this.Button_Dpad_6.IconRightVisible = true;
            this.Button_Dpad_6.IconRightZoom = 0D;
            this.Button_Dpad_6.IconVisible = true;
            this.Button_Dpad_6.IconZoom = 90D;
            this.Button_Dpad_6.IsTab = false;
            this.Button_Dpad_6.Location = new System.Drawing.Point(254, 141);
            this.Button_Dpad_6.Name = "Button_Dpad_6";
            this.Button_Dpad_6.Normalcolor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.Button_Dpad_6.OnHovercolor = System.Drawing.Color.DarkSeaGreen;
            this.Button_Dpad_6.OnHoverTextColor = System.Drawing.Color.Black;
            this.Button_Dpad_6.selected = false;
            this.Button_Dpad_6.Size = new System.Drawing.Size(94, 48);
            this.Button_Dpad_6.TabIndex = 5;
            this.Button_Dpad_6.Text = "6";
            this.Button_Dpad_6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.Button_Dpad_6.Textcolor = System.Drawing.Color.Black;
            this.Button_Dpad_6.TextFont = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Button_Dpad_6.Click += new System.EventHandler(this.Button_Dpad_Click);
            // 
            // Button_Dpad_7
            // 
            this.Button_Dpad_7.Activecolor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(139)))), ((int)(((byte)(87)))));
            this.Button_Dpad_7.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.Button_Dpad_7.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.Button_Dpad_7.BorderRadius = 0;
            this.Button_Dpad_7.ButtonText = "7";
            this.Button_Dpad_7.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Button_Dpad_7.DisabledColor = System.Drawing.Color.Gray;
            this.Button_Dpad_7.Font = new System.Drawing.Font("Bell MT", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Button_Dpad_7.Iconcolor = System.Drawing.Color.Transparent;
            this.Button_Dpad_7.Iconimage = null;
            this.Button_Dpad_7.Iconimage_right = null;
            this.Button_Dpad_7.Iconimage_right_Selected = null;
            this.Button_Dpad_7.Iconimage_Selected = null;
            this.Button_Dpad_7.IconMarginLeft = 0;
            this.Button_Dpad_7.IconMarginRight = 0;
            this.Button_Dpad_7.IconRightVisible = true;
            this.Button_Dpad_7.IconRightZoom = 0D;
            this.Button_Dpad_7.IconVisible = true;
            this.Button_Dpad_7.IconZoom = 90D;
            this.Button_Dpad_7.IsTab = false;
            this.Button_Dpad_7.Location = new System.Drawing.Point(54, 195);
            this.Button_Dpad_7.Name = "Button_Dpad_7";
            this.Button_Dpad_7.Normalcolor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.Button_Dpad_7.OnHovercolor = System.Drawing.Color.DarkSeaGreen;
            this.Button_Dpad_7.OnHoverTextColor = System.Drawing.Color.Black;
            this.Button_Dpad_7.selected = false;
            this.Button_Dpad_7.Size = new System.Drawing.Size(94, 48);
            this.Button_Dpad_7.TabIndex = 6;
            this.Button_Dpad_7.Text = "7";
            this.Button_Dpad_7.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.Button_Dpad_7.Textcolor = System.Drawing.Color.Black;
            this.Button_Dpad_7.TextFont = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Button_Dpad_7.Click += new System.EventHandler(this.Button_Dpad_Click);
            // 
            // Button_Dpad_8
            // 
            this.Button_Dpad_8.Activecolor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(139)))), ((int)(((byte)(87)))));
            this.Button_Dpad_8.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.Button_Dpad_8.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.Button_Dpad_8.BorderRadius = 0;
            this.Button_Dpad_8.ButtonText = "8";
            this.Button_Dpad_8.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Button_Dpad_8.DisabledColor = System.Drawing.Color.Gray;
            this.Button_Dpad_8.Font = new System.Drawing.Font("Bell MT", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Button_Dpad_8.Iconcolor = System.Drawing.Color.Transparent;
            this.Button_Dpad_8.Iconimage = null;
            this.Button_Dpad_8.Iconimage_right = null;
            this.Button_Dpad_8.Iconimage_right_Selected = null;
            this.Button_Dpad_8.Iconimage_Selected = null;
            this.Button_Dpad_8.IconMarginLeft = 0;
            this.Button_Dpad_8.IconMarginRight = 0;
            this.Button_Dpad_8.IconRightVisible = true;
            this.Button_Dpad_8.IconRightZoom = 0D;
            this.Button_Dpad_8.IconVisible = true;
            this.Button_Dpad_8.IconZoom = 90D;
            this.Button_Dpad_8.IsTab = false;
            this.Button_Dpad_8.Location = new System.Drawing.Point(154, 195);
            this.Button_Dpad_8.Name = "Button_Dpad_8";
            this.Button_Dpad_8.Normalcolor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.Button_Dpad_8.OnHovercolor = System.Drawing.Color.DarkSeaGreen;
            this.Button_Dpad_8.OnHoverTextColor = System.Drawing.Color.Black;
            this.Button_Dpad_8.selected = false;
            this.Button_Dpad_8.Size = new System.Drawing.Size(94, 48);
            this.Button_Dpad_8.TabIndex = 7;
            this.Button_Dpad_8.Text = "8";
            this.Button_Dpad_8.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.Button_Dpad_8.Textcolor = System.Drawing.Color.Black;
            this.Button_Dpad_8.TextFont = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Button_Dpad_8.Click += new System.EventHandler(this.Button_Dpad_Click);
            // 
            // Button_Dpad_9
            // 
            this.Button_Dpad_9.Activecolor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(139)))), ((int)(((byte)(87)))));
            this.Button_Dpad_9.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.Button_Dpad_9.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.Button_Dpad_9.BorderRadius = 0;
            this.Button_Dpad_9.ButtonText = "9";
            this.Button_Dpad_9.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Button_Dpad_9.DisabledColor = System.Drawing.Color.Gray;
            this.Button_Dpad_9.Font = new System.Drawing.Font("Bell MT", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Button_Dpad_9.Iconcolor = System.Drawing.Color.Transparent;
            this.Button_Dpad_9.Iconimage = null;
            this.Button_Dpad_9.Iconimage_right = null;
            this.Button_Dpad_9.Iconimage_right_Selected = null;
            this.Button_Dpad_9.Iconimage_Selected = null;
            this.Button_Dpad_9.IconMarginLeft = 0;
            this.Button_Dpad_9.IconMarginRight = 0;
            this.Button_Dpad_9.IconRightVisible = true;
            this.Button_Dpad_9.IconRightZoom = 0D;
            this.Button_Dpad_9.IconVisible = true;
            this.Button_Dpad_9.IconZoom = 90D;
            this.Button_Dpad_9.IsTab = false;
            this.Button_Dpad_9.Location = new System.Drawing.Point(254, 195);
            this.Button_Dpad_9.Name = "Button_Dpad_9";
            this.Button_Dpad_9.Normalcolor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.Button_Dpad_9.OnHovercolor = System.Drawing.Color.DarkSeaGreen;
            this.Button_Dpad_9.OnHoverTextColor = System.Drawing.Color.Black;
            this.Button_Dpad_9.selected = false;
            this.Button_Dpad_9.Size = new System.Drawing.Size(94, 48);
            this.Button_Dpad_9.TabIndex = 8;
            this.Button_Dpad_9.Text = "9";
            this.Button_Dpad_9.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.Button_Dpad_9.Textcolor = System.Drawing.Color.Black;
            this.Button_Dpad_9.TextFont = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Button_Dpad_9.Click += new System.EventHandler(this.Button_Dpad_Click);
            // 
            // Button_Dpad_star
            // 
            this.Button_Dpad_star.Activecolor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(139)))), ((int)(((byte)(87)))));
            this.Button_Dpad_star.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.Button_Dpad_star.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.Button_Dpad_star.BorderRadius = 0;
            this.Button_Dpad_star.ButtonText = "*";
            this.Button_Dpad_star.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Button_Dpad_star.DisabledColor = System.Drawing.Color.Gray;
            this.Button_Dpad_star.Font = new System.Drawing.Font("Bell MT", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Button_Dpad_star.Iconcolor = System.Drawing.Color.Transparent;
            this.Button_Dpad_star.Iconimage = null;
            this.Button_Dpad_star.Iconimage_right = null;
            this.Button_Dpad_star.Iconimage_right_Selected = null;
            this.Button_Dpad_star.Iconimage_Selected = null;
            this.Button_Dpad_star.IconMarginLeft = 0;
            this.Button_Dpad_star.IconMarginRight = 0;
            this.Button_Dpad_star.IconRightVisible = true;
            this.Button_Dpad_star.IconRightZoom = 0D;
            this.Button_Dpad_star.IconVisible = true;
            this.Button_Dpad_star.IconZoom = 90D;
            this.Button_Dpad_star.IsTab = false;
            this.Button_Dpad_star.Location = new System.Drawing.Point(54, 249);
            this.Button_Dpad_star.Name = "Button_Dpad_star";
            this.Button_Dpad_star.Normalcolor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.Button_Dpad_star.OnHovercolor = System.Drawing.Color.DarkSeaGreen;
            this.Button_Dpad_star.OnHoverTextColor = System.Drawing.Color.Black;
            this.Button_Dpad_star.selected = false;
            this.Button_Dpad_star.Size = new System.Drawing.Size(94, 48);
            this.Button_Dpad_star.TabIndex = 9;
            this.Button_Dpad_star.Text = "*";
            this.Button_Dpad_star.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.Button_Dpad_star.Textcolor = System.Drawing.Color.Black;
            this.Button_Dpad_star.TextFont = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Button_Dpad_star.Click += new System.EventHandler(this.Button_Dpad_Click);
            // 
            // Button_Dpad_0
            // 
            this.Button_Dpad_0.Activecolor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(139)))), ((int)(((byte)(87)))));
            this.Button_Dpad_0.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.Button_Dpad_0.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.Button_Dpad_0.BorderRadius = 0;
            this.Button_Dpad_0.ButtonText = "0";
            this.Button_Dpad_0.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Button_Dpad_0.DisabledColor = System.Drawing.Color.Gray;
            this.Button_Dpad_0.Font = new System.Drawing.Font("Bell MT", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Button_Dpad_0.Iconcolor = System.Drawing.Color.Transparent;
            this.Button_Dpad_0.Iconimage = null;
            this.Button_Dpad_0.Iconimage_right = null;
            this.Button_Dpad_0.Iconimage_right_Selected = null;
            this.Button_Dpad_0.Iconimage_Selected = null;
            this.Button_Dpad_0.IconMarginLeft = 0;
            this.Button_Dpad_0.IconMarginRight = 0;
            this.Button_Dpad_0.IconRightVisible = true;
            this.Button_Dpad_0.IconRightZoom = 0D;
            this.Button_Dpad_0.IconVisible = true;
            this.Button_Dpad_0.IconZoom = 90D;
            this.Button_Dpad_0.IsTab = false;
            this.Button_Dpad_0.Location = new System.Drawing.Point(154, 249);
            this.Button_Dpad_0.Name = "Button_Dpad_0";
            this.Button_Dpad_0.Normalcolor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.Button_Dpad_0.OnHovercolor = System.Drawing.Color.DarkSeaGreen;
            this.Button_Dpad_0.OnHoverTextColor = System.Drawing.Color.Black;
            this.Button_Dpad_0.selected = false;
            this.Button_Dpad_0.Size = new System.Drawing.Size(94, 48);
            this.Button_Dpad_0.TabIndex = 10;
            this.Button_Dpad_0.Text = "0";
            this.Button_Dpad_0.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.Button_Dpad_0.Textcolor = System.Drawing.Color.Black;
            this.Button_Dpad_0.TextFont = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Button_Dpad_0.Click += new System.EventHandler(this.Button_Dpad_Click);
            // 
            // Button_Dpad_hash
            // 
            this.Button_Dpad_hash.Activecolor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(139)))), ((int)(((byte)(87)))));
            this.Button_Dpad_hash.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.Button_Dpad_hash.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.Button_Dpad_hash.BorderRadius = 0;
            this.Button_Dpad_hash.ButtonText = "#";
            this.Button_Dpad_hash.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Button_Dpad_hash.DisabledColor = System.Drawing.Color.Gray;
            this.Button_Dpad_hash.Font = new System.Drawing.Font("Bell MT", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Button_Dpad_hash.Iconcolor = System.Drawing.Color.Transparent;
            this.Button_Dpad_hash.Iconimage = null;
            this.Button_Dpad_hash.Iconimage_right = null;
            this.Button_Dpad_hash.Iconimage_right_Selected = null;
            this.Button_Dpad_hash.Iconimage_Selected = null;
            this.Button_Dpad_hash.IconMarginLeft = 0;
            this.Button_Dpad_hash.IconMarginRight = 0;
            this.Button_Dpad_hash.IconRightVisible = true;
            this.Button_Dpad_hash.IconRightZoom = 0D;
            this.Button_Dpad_hash.IconVisible = true;
            this.Button_Dpad_hash.IconZoom = 90D;
            this.Button_Dpad_hash.IsTab = false;
            this.Button_Dpad_hash.Location = new System.Drawing.Point(254, 249);
            this.Button_Dpad_hash.Name = "Button_Dpad_hash";
            this.Button_Dpad_hash.Normalcolor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.Button_Dpad_hash.OnHovercolor = System.Drawing.Color.DarkSeaGreen;
            this.Button_Dpad_hash.OnHoverTextColor = System.Drawing.Color.Black;
            this.Button_Dpad_hash.selected = false;
            this.Button_Dpad_hash.Size = new System.Drawing.Size(94, 48);
            this.Button_Dpad_hash.TabIndex = 11;
            this.Button_Dpad_hash.Text = "#";
            this.Button_Dpad_hash.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.Button_Dpad_hash.Textcolor = System.Drawing.Color.Black;
            this.Button_Dpad_hash.TextFont = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Button_Dpad_hash.Click += new System.EventHandler(this.Button_Dpad_Click);
            // 
            // bunifuFlatButton15
            // 
            this.bunifuFlatButton15.Activecolor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(139)))), ((int)(((byte)(87)))));
            this.bunifuFlatButton15.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.bunifuFlatButton15.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.bunifuFlatButton15.BorderRadius = 0;
            this.bunifuFlatButton15.ButtonText = "Call / Answer";
            this.bunifuFlatButton15.Cursor = System.Windows.Forms.Cursors.Hand;
            this.bunifuFlatButton15.DisabledColor = System.Drawing.Color.Gray;
            this.bunifuFlatButton15.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bunifuFlatButton15.Iconcolor = System.Drawing.Color.Transparent;
            this.bunifuFlatButton15.Iconimage = ((System.Drawing.Image)(resources.GetObject("bunifuFlatButton15.Iconimage")));
            this.bunifuFlatButton15.Iconimage_right = null;
            this.bunifuFlatButton15.Iconimage_right_Selected = null;
            this.bunifuFlatButton15.Iconimage_Selected = null;
            this.bunifuFlatButton15.IconMarginLeft = 0;
            this.bunifuFlatButton15.IconMarginRight = 0;
            this.bunifuFlatButton15.IconRightVisible = true;
            this.bunifuFlatButton15.IconRightZoom = 0D;
            this.bunifuFlatButton15.IconVisible = true;
            this.bunifuFlatButton15.IconZoom = 90D;
            this.bunifuFlatButton15.IsTab = false;
            this.bunifuFlatButton15.Location = new System.Drawing.Point(6, 328);
            this.bunifuFlatButton15.Name = "bunifuFlatButton15";
            this.bunifuFlatButton15.Normalcolor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.bunifuFlatButton15.OnHovercolor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.bunifuFlatButton15.OnHoverTextColor = System.Drawing.Color.Black;
            this.bunifuFlatButton15.selected = false;
            this.bunifuFlatButton15.Size = new System.Drawing.Size(142, 48);
            this.bunifuFlatButton15.TabIndex = 12;
            this.bunifuFlatButton15.Text = "Call / Answer";
            this.bunifuFlatButton15.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.bunifuFlatButton15.Textcolor = System.Drawing.Color.Black;
            this.bunifuFlatButton15.TextFont = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            // 
            // bunifuFlatButton14
            // 
            this.bunifuFlatButton14.Activecolor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(139)))), ((int)(((byte)(87)))));
            this.bunifuFlatButton14.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.bunifuFlatButton14.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.bunifuFlatButton14.BorderRadius = 0;
            this.bunifuFlatButton14.ButtonText = "Redial";
            this.bunifuFlatButton14.Cursor = System.Windows.Forms.Cursors.Hand;
            this.bunifuFlatButton14.DisabledColor = System.Drawing.Color.Gray;
            this.bunifuFlatButton14.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bunifuFlatButton14.Iconcolor = System.Drawing.Color.Transparent;
            this.bunifuFlatButton14.Iconimage = null;
            this.bunifuFlatButton14.Iconimage_right = null;
            this.bunifuFlatButton14.Iconimage_right_Selected = null;
            this.bunifuFlatButton14.Iconimage_Selected = null;
            this.bunifuFlatButton14.IconMarginLeft = 0;
            this.bunifuFlatButton14.IconMarginRight = 0;
            this.bunifuFlatButton14.IconRightVisible = true;
            this.bunifuFlatButton14.IconRightZoom = 0D;
            this.bunifuFlatButton14.IconVisible = true;
            this.bunifuFlatButton14.IconZoom = 90D;
            this.bunifuFlatButton14.IsTab = false;
            this.bunifuFlatButton14.Location = new System.Drawing.Point(154, 328);
            this.bunifuFlatButton14.Name = "bunifuFlatButton14";
            this.bunifuFlatButton14.Normalcolor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.bunifuFlatButton14.OnHovercolor = System.Drawing.Color.Teal;
            this.bunifuFlatButton14.OnHoverTextColor = System.Drawing.Color.White;
            this.bunifuFlatButton14.selected = false;
            this.bunifuFlatButton14.Size = new System.Drawing.Size(94, 48);
            this.bunifuFlatButton14.TabIndex = 13;
            this.bunifuFlatButton14.Text = "Redial";
            this.bunifuFlatButton14.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.bunifuFlatButton14.Textcolor = System.Drawing.Color.Black;
            this.bunifuFlatButton14.TextFont = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            // 
            // bunifuFlatButton13
            // 
            this.bunifuFlatButton13.Activecolor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(139)))), ((int)(((byte)(87)))));
            this.bunifuFlatButton13.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.bunifuFlatButton13.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.bunifuFlatButton13.BorderRadius = 0;
            this.bunifuFlatButton13.ButtonText = "End / Reject";
            this.bunifuFlatButton13.Cursor = System.Windows.Forms.Cursors.Hand;
            this.bunifuFlatButton13.DisabledColor = System.Drawing.Color.Gray;
            this.bunifuFlatButton13.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bunifuFlatButton13.Iconcolor = System.Drawing.Color.Transparent;
            this.bunifuFlatButton13.Iconimage = ((System.Drawing.Image)(resources.GetObject("bunifuFlatButton13.Iconimage")));
            this.bunifuFlatButton13.Iconimage_right = null;
            this.bunifuFlatButton13.Iconimage_right_Selected = null;
            this.bunifuFlatButton13.Iconimage_Selected = null;
            this.bunifuFlatButton13.IconMarginLeft = 0;
            this.bunifuFlatButton13.IconMarginRight = 0;
            this.bunifuFlatButton13.IconRightVisible = true;
            this.bunifuFlatButton13.IconRightZoom = 0D;
            this.bunifuFlatButton13.IconVisible = true;
            this.bunifuFlatButton13.IconZoom = 90D;
            this.bunifuFlatButton13.IsTab = false;
            this.bunifuFlatButton13.Location = new System.Drawing.Point(254, 328);
            this.bunifuFlatButton13.Name = "bunifuFlatButton13";
            this.bunifuFlatButton13.Normalcolor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.bunifuFlatButton13.OnHovercolor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.bunifuFlatButton13.OnHoverTextColor = System.Drawing.Color.Black;
            this.bunifuFlatButton13.selected = false;
            this.bunifuFlatButton13.Size = new System.Drawing.Size(132, 48);
            this.bunifuFlatButton13.TabIndex = 14;
            this.bunifuFlatButton13.Text = "End / Reject";
            this.bunifuFlatButton13.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.bunifuFlatButton13.Textcolor = System.Drawing.Color.Black;
            this.bunifuFlatButton13.TextFont = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            // 
            // panel_Dpad
            // 
            this.panel_Dpad.Controls.Add(this.bunifuMaterialTextbox1);
            this.panel_Dpad.Controls.Add(this.bunifuFlatButton13);
            this.panel_Dpad.Controls.Add(this.bunifuFlatButton14);
            this.panel_Dpad.Controls.Add(this.bunifuFlatButton15);
            this.panel_Dpad.Controls.Add(this.Button_Dpad_hash);
            this.panel_Dpad.Controls.Add(this.Button_Dpad_0);
            this.panel_Dpad.Controls.Add(this.Button_Dpad_star);
            this.panel_Dpad.Controls.Add(this.Button_Dpad_9);
            this.panel_Dpad.Controls.Add(this.Button_Dpad_8);
            this.panel_Dpad.Controls.Add(this.Button_Dpad_7);
            this.panel_Dpad.Controls.Add(this.Button_Dpad_6);
            this.panel_Dpad.Controls.Add(this.Button_Dpad_5);
            this.panel_Dpad.Controls.Add(this.Button_Dpad_4);
            this.panel_Dpad.Controls.Add(this.Button_Dpad_3);
            this.panel_Dpad.Controls.Add(this.Button_Dpad_2);
            this.panel_Dpad.Controls.Add(this.Button_Dpad_1);
            this.panel_Dpad.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.panel_Dpad.Location = new System.Drawing.Point(4, 242);
            this.panel_Dpad.Name = "panel_Dpad";
            this.panel_Dpad.Size = new System.Drawing.Size(395, 405);
            this.panel_Dpad.TabIndex = 2;
            // 
            // bunifuMaterialTextbox1
            // 
            this.bunifuMaterialTextbox1.BackColor = System.Drawing.Color.Silver;
            this.bunifuMaterialTextbox1.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.bunifuMaterialTextbox1.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.bunifuMaterialTextbox1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.bunifuMaterialTextbox1.HintForeColor = System.Drawing.Color.Empty;
            this.bunifuMaterialTextbox1.HintText = "Destination number";
            this.bunifuMaterialTextbox1.isPassword = false;
            this.bunifuMaterialTextbox1.LineFocusedColor = System.Drawing.Color.Blue;
            this.bunifuMaterialTextbox1.LineIdleColor = System.Drawing.Color.Gray;
            this.bunifuMaterialTextbox1.LineMouseHoverColor = System.Drawing.Color.Blue;
            this.bunifuMaterialTextbox1.LineThickness = 3;
            this.bunifuMaterialTextbox1.Location = new System.Drawing.Point(54, 29);
            this.bunifuMaterialTextbox1.Margin = new System.Windows.Forms.Padding(4);
            this.bunifuMaterialTextbox1.Name = "bunifuMaterialTextbox1";
            this.bunifuMaterialTextbox1.Size = new System.Drawing.Size(295, 33);
            this.bunifuMaterialTextbox1.TabIndex = 15;
            this.bunifuMaterialTextbox1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // panel_Log
            // 
            this.panel_Log.Controls.Add(this.listBox1);
            this.panel_Log.Location = new System.Drawing.Point(4, 242);
            this.panel_Log.Name = "panel_Log";
            this.panel_Log.Size = new System.Drawing.Size(395, 405);
            this.panel_Log.TabIndex = 16;
            // 
            // listBox1
            // 
            this.listBox1.FormattingEnabled = true;
            this.listBox1.Items.AddRange(new object[] {
            "Even log for current run :",
            "--------------------------"});
            this.listBox1.Location = new System.Drawing.Point(4, 4);
            this.listBox1.Name = "listBox1";
            this.listBox1.Size = new System.Drawing.Size(388, 394);
            this.listBox1.TabIndex = 0;
            // 
            // panel_History
            // 
            this.panel_History.Controls.Add(this.listBox_History);
            this.panel_History.Location = new System.Drawing.Point(4, 242);
            this.panel_History.Name = "panel_History";
            this.panel_History.Size = new System.Drawing.Size(395, 405);
            this.panel_History.TabIndex = 17;
            // 
            // bunifu_Panel_Bottom
            // 
            this.bunifu_Panel_Bottom.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("bunifu_Panel_Bottom.BackgroundImage")));
            this.bunifu_Panel_Bottom.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.bunifu_Panel_Bottom.GradientBottomLeft = System.Drawing.Color.White;
            this.bunifu_Panel_Bottom.GradientBottomRight = System.Drawing.Color.White;
            this.bunifu_Panel_Bottom.GradientTopLeft = System.Drawing.Color.White;
            this.bunifu_Panel_Bottom.GradientTopRight = System.Drawing.Color.White;
            this.bunifu_Panel_Bottom.Location = new System.Drawing.Point(-1, 646);
            this.bunifu_Panel_Bottom.Name = "bunifu_Panel_Bottom";
            this.bunifu_Panel_Bottom.Quality = 10;
            this.bunifu_Panel_Bottom.Size = new System.Drawing.Size(405, 54);
            this.bunifu_Panel_Bottom.TabIndex = 18;
            // 
            // listBox_History
            // 
            this.listBox_History.Font = new System.Drawing.Font("Century Gothic", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.listBox_History.FormattingEnabled = true;
            this.listBox_History.ItemHeight = 33;
            this.listBox_History.Items.AddRange(new object[] {
            "History Event 1",
            "History Event 2",
            "Outgoing Call 1",
            "Incoming Call 1"});
            this.listBox_History.Location = new System.Drawing.Point(3, 4);
            this.listBox_History.Name = "listBox_History";
            this.listBox_History.Size = new System.Drawing.Size(389, 400);
            this.listBox_History.TabIndex = 0;
            // 
            // Main_Screen
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(404, 701);
            this.Controls.Add(this.bunifu_Panel_Bottom);
            this.Controls.Add(this.panel_Log);
            this.Controls.Add(this.panel_status);
            this.Controls.Add(this.panel_Dpad);
            this.Controls.Add(this.panel_icons);
            this.Controls.Add(this.panel_History);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Main_Screen";
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.Text = "Tamarin VoIP Client";
            this.Load += new System.EventHandler(this.Main_Screen_Load);
            this.panel_icons.ResumeLayout(false);
            this.panel_status.ResumeLayout(false);
            this.panel_Login.ResumeLayout(false);
            this.panel_Login.PerformLayout();
            this.panel_Dpad.ResumeLayout(false);
            this.panel_Log.ResumeLayout(false);
            this.panel_History.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel_icons;
        private Bunifu.Framework.UI.BunifuFlatButton Top_btn_dpad;
        private Bunifu.Framework.UI.BunifuFlatButton Top_btn_cont;
        private Bunifu.Framework.UI.BunifuFlatButton Top_btn_hist;
        private Bunifu.Framework.UI.BunifuFlatButton Top_btn_log;
        private Bunifu.Framework.UI.BunifuFlatButton Top_btn_set;
        private System.Windows.Forms.Panel panel_status;
        private Bunifu.Framework.UI.BunifuFlatButton Button_Dpad_1;
        private Bunifu.Framework.UI.BunifuFlatButton Button_Dpad_2;
        private Bunifu.Framework.UI.BunifuFlatButton Button_Dpad_3;
        private Bunifu.Framework.UI.BunifuFlatButton Button_Dpad_4;
        private Bunifu.Framework.UI.BunifuFlatButton Button_Dpad_5;
        private Bunifu.Framework.UI.BunifuFlatButton Button_Dpad_6;
        private Bunifu.Framework.UI.BunifuFlatButton Button_Dpad_7;
        private Bunifu.Framework.UI.BunifuFlatButton Button_Dpad_8;
        private Bunifu.Framework.UI.BunifuFlatButton Button_Dpad_9;
        private Bunifu.Framework.UI.BunifuFlatButton Button_Dpad_star;
        private Bunifu.Framework.UI.BunifuFlatButton Button_Dpad_0;
        private Bunifu.Framework.UI.BunifuFlatButton Button_Dpad_hash;
        private Bunifu.Framework.UI.BunifuFlatButton bunifuFlatButton15;
        private Bunifu.Framework.UI.BunifuFlatButton bunifuFlatButton14;
        private Bunifu.Framework.UI.BunifuFlatButton bunifuFlatButton13;
        private System.Windows.Forms.Panel panel_Dpad;
        private System.Windows.Forms.Panel panel_Login;
        private Bunifu.Framework.UI.BunifuCustomLabel bunifuCustomLabel1;
        private Bunifu.Framework.UI.BunifuMetroTextbox bunifuMetroTextbox2;
        private Bunifu.Framework.UI.BunifuMetroTextbox bunifuMetroTextbox1;
        private Bunifu.Framework.UI.BunifuCustomLabel bunifuCustomLabel3;
        private Bunifu.Framework.UI.BunifuCustomLabel bunifuCustomLabel2;
        private Bunifu.Framework.UI.BunifuMetroTextbox bunifuMetroTextbox3;
        private Bunifu.Framework.UI.BunifuCustomLabel bunifuCustomLabel4;
        private Bunifu.Framework.UI.BunifuThinButton2 RegButtin;
        private Bunifu.Framework.UI.BunifuMaterialTextbox bunifuMaterialTextbox1;
        private System.Windows.Forms.Panel panel_Log;
        private System.Windows.Forms.ListBox listBox1;
        private System.Windows.Forms.Panel panel_History;
        private System.Windows.Forms.ListBox listBox_History;
        private Bunifu.Framework.UI.BunifuGradientPanel bunifu_Panel_Bottom;
    }
}